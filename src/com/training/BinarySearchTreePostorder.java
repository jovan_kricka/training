package com.training;

import com.training.common.Node;

import static com.training.common.TestData.buildBinarySearchTree;

public class BinarySearchTreePostorder {

    public static void main(String[] args) {
        Node binarySearchTree = buildBinarySearchTree();
        postOrderVisit(binarySearchTree);
    }

    private static void postOrderVisit(Node node) {
        if (node == null) {
            return;
        }
        postOrderVisit(node.getLeft());
        postOrderVisit(node.getRight());
        System.out.println(node.getValue());
    }

}
