package com.training;

import com.training.common.Node;

import static com.training.common.TestData.buildBinarySearchTree;

public class BinarySearchTreeInorder {

    public static void main(String[] args) {
        Node binarySearchTree = buildBinarySearchTree();
        inOrderVisit(binarySearchTree);
    }

    private static void inOrderVisit(Node node) {
        if (node == null) {
            return;
        }
        inOrderVisit(node.getLeft());
        System.out.println(node.getValue());
        inOrderVisit(node.getRight());
    }

}
