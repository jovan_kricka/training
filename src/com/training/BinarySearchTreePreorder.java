package com.training;

import com.training.common.Node;

import static com.training.common.TestData.buildBinarySearchTree;

public class BinarySearchTreePreorder {

    public static void main(String[] args) {
        Node binarySearchTree = buildBinarySearchTree();
        preOrderVisit(binarySearchTree);
    }

    private static void preOrderVisit(Node node) {
        if (node == null) {
            return;
        }
        System.out.println(node.getValue());
        preOrderVisit(node.getLeft());
        preOrderVisit(node.getRight());
    }

}
