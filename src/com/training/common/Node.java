package com.training.common;

public class Node {

    private final int value;
    private final Node left;
    private final Node right;

    private Node(int value, Node left, Node right) {
        this.value = value;
        this.left = left;
        this.right = right;
    }

    public static Node of(int value, Node left, Node right) {
        return new Node(value, left, right);
    }

    public int getValue() {
        return value;
    }

    public Node getLeft() {
        return left;
    }

    public Node getRight() {
        return right;
    }

    public boolean hasLeft() {
        return left != null;
    }

    public boolean hasRight() {
        return right != null;
    }

}
