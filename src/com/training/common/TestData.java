package com.training.common;

public class TestData {

    private TestData() {
    }

    //@formatter:off
    /**
     * @return {@link Node} containing a binary search tree in following form:
     *                                      10
     *              6                                       17
     *     3                7                      12                   22
     *                              9          11       15          20      31
     *                          8
     */
    //@formatter:on
    public static Node buildBinarySearchTree() {
        return Node.of(10,
                Node.of(6,
                        Node.of(3, null, null),
                        Node.of(7,
                                null,
                                Node.of(9,
                                        Node.of(8, null, null),
                                        null))),
                Node.of(17,
                        Node.of(12,
                                Node.of(11, null, null),
                                Node.of(15, null, null)),
                        Node.of(22,
                                Node.of(20, null, null),
                                Node.of(31, null, null))));
    }

}
