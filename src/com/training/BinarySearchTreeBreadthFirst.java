package com.training;

import com.training.common.Node;

import java.util.*;

import static com.training.common.TestData.buildBinarySearchTree;

public class BinarySearchTreeBreadthFirst {

    public static void main(String[] args) {
        Node binarySearchTree = buildBinarySearchTree();
        Deque<Node> queue = new LinkedList<>();
        queue.add(binarySearchTree);
        breadthFirstVisit(queue);
    }

    private static void breadthFirstVisit(Deque<Node> queue) {
        if (queue.isEmpty()) {
            return;
        }
        Node node = queue.removeFirst();
        System.out.println(node.getValue());
        if (node.hasLeft()) {
            queue.add(node.getLeft());
        }
        if (node.hasRight()) {
            queue.add(node.getRight());
        }
        breadthFirstVisit(queue);
    }

}
